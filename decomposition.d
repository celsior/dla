module decomposition;

import std.stdio;
import std.math;

import matrix;

// Cormen p.845
void LU(T)(Matrix!T M, out Matrix!T L, out Matrix!T U)
in
{
  assert(M.rows == M.cols, "Not a square matrix");
}
body
{
  auto A = Matrix!T(M);
  size_t n = A.rows;

  L = Matrix!T(n, n);
  U = Matrix!T(n, n);

  foreach (k; 0 .. n)
    {
      L[k,k] = 1;
      U[k,k] = A[k,k];
      foreach (i; k+1 .. n)
        {
          L[i,k] = A[i,k] / A[k,k];
          U[k,i] = A[k,i];
        }

      foreach (i; k+1 .. n)
        foreach (j; k+1 .. n)
          A[i,j] -= L[i,k] * U[k,j];
    }
}

unittest
{
  auto A = Matrix!double(4, 4, [2,  3,  1,  5,
                                6, 13,  5, 19,
                                2, 19, 10, 23,
                                4, 10, 11, 31]);
  auto L = Matrix!double();
  auto U = Matrix!double();
  LU(A, L, U);
  assert(L == [1, 0, 0, 0,
               3, 1, 0, 0,
               1, 4, 1, 0,
               2, 1, 7, 1]);
  assert(U == [2, 3, 1, 5,
               0, 4, 2, 4,
               0, 0, 1, 2,
               0, 0, 0, 3]);
}


// Cormen p.848
void LUP(T)(Matrix!T M, out Matrix!T L, out Matrix!T U, out Matrix!size_t P)
in
{
  assert(M.rows == M.cols, "Not a square matrix");
}
body
{
  auto A = Matrix!T(M);
  size_t n = A.rows;

  P = Matrix!size_t(n, 1);

  foreach (i; 0 .. n)
    P[i] = i;

  bool is_strictly_diagonally_dominant = M.is_strictly_diagonally_dominant();

  foreach (k; 0 .. n)
    {
      if (!is_strictly_diagonally_dominant)
        {
          size_t max_elem_index = k;
          foreach (i; k+1 .. n)
            if (abs(A[max_elem_index,k]) < abs(A[i, k]))
              max_elem_index = i;

          P.swap_rows(k, max_elem_index);
          A.swap_rows(k, max_elem_index);
        }

      foreach (i; k+1 .. n)
        {
          A[i,k] /= A[k,k];
          foreach (j; k+1 .. n)
            A[i,j] -= A[i,k] * A[k,j];
        }
    }

  L = Matrix!T(n, n);
  U = Matrix!T(n, n);

  foreach (i; 0 .. n)
    foreach (j; i .. n)
      {
        if (i == j)
          L[j,i] = 1;
        else
          L[j,i] = A[j,i];

        U[i,j] = A[i, j];
      }
}

unittest
{
  auto A = Matrix!double(4, 4, [ 2,  0,   2, 0.6,
                                 3,  3,   4,  -2,
                                 5,  5,   4,   2,
                                -1, -2, 3.4,  -1]);
  auto L = Matrix!double();
  auto U = Matrix!double();
  auto P = Matrix!size_t();
  LUP(A, L, U, P);

  L.modify_each( (double v) => cast(double)round(v*1000)/1000 );
  assert(L == [   1,   0,   0, 0,
                0.4,   1,   0, 0,
               -0.2, 0.5,   1, 0,
                0.6,   0, 0.4, 1]);

  U.modify_each( (double v) => cast(double)round(v*1000)/1000 );
  assert(U == [5,  5,   4,    2,
               0, -2, 0.4, -0.2,
               0,  0,   4, -0.5,
               0,  0,   0,   -3]);

  assert(P == [2,
               0,
               3,
               1]);
}

Matrix!T forward_substitution(T)(Matrix!T A, Matrix!T b)
{
  size_t m = A.rows;
  auto x = Matrix!T(m, 1);

  foreach (size_t i; 0 .. b.rows)
    {
      x[i] = b[i];
      foreach (size_t j; 0..i)
        x[i] -= A[i, j] * x[j];
      x[i] /= A[i, i];
    }

  return x;
}

Matrix!T backward_substitution(T)(Matrix!T A, Matrix!T b)
{
  size_t m = A.rows;
  auto x = Matrix!T(m, 1);

  foreach_reverse (size_t i; 0 .. m)
    {
      x[i] = b[i];
      foreach (size_t j; i+1 .. m)
        x[i] -= A[i, j] * x[j];
      x[i] /= A[i, i];
    }

  return x;
}

Matrix!T LUPEval(T)(Matrix!T L, Matrix!T U, Matrix!size_t P, Matrix!T b)
{
  auto Pb = Matrix!T(b.rows, 1);
  foreach (i; 0 .. P.rows)
    Pb[i] = b[P[i]];

  auto y = forward_substitution(L, Pb);
  return backward_substitution(U, y);
}

Matrix!T solve(T)(Matrix!T A, Matrix!T b)
{
  assert (A.rows == A.cols, "Not a square matrix");
  assert (b.cols == 1, "b is not a column vector");
  assert (b.rows == A.rows, "Dimension mismatch");

  auto L = Matrix!double();
  auto U = Matrix!double();
  auto P = Matrix!size_t();
  LUP(A, L, U, P);

  return LUPEval(L, U, P, b);
}
unittest
{
  auto M1 = Matrix!double(3,3, [1, 2, 4,
                                8, 2, 6,
                                8, 0, 4]);
  auto M2 = Matrix!double(3,1, [1,2,2]);

  assert(solve(M1, M2) == [0, -0.5, 0.5]);
}

Matrix!T inv(T)(Matrix!T A)
{
  assert (A.rows == A.cols, "Not a square matrix");

  auto res = Matrix!T(A.rows, A.cols);

  auto L = Matrix!double();
  auto U = Matrix!double();
  auto P = Matrix!size_t();
  LUP(A, L, U, P);

  foreach (size_t i; 0 .. A.rows)
    {
      auto b = Matrix!T(A.rows, 1);
      b[i] = 1;
      res.col(i) = LUPEval(L, U, P, b);
    }

  return res;
}
unittest
{
  auto M1 = Matrix!double(3,3, [1, 2, 4,
                                10, 2, 6,
                                8, 0, 4]);

  auto M2 = inv(M1);
  M2.modify_each( (double v) => cast(double)round(v*1000)/1000 );

  assert (M2 == [-0.2,  0.2, -0.1,
                 -0.2,  0.7, -0.85,
                  0.4, -0.4,  0.45]);
}


Matrix!T compute_householder_vector(T)(Matrix!T x)
{
  assert(x.cols == 1, "x is not a column vector");

  auto e1 = Matrix!T(x.rows, 1, 0);
  e1[0] = 1;

  auto v = x + sgn(x[0]) * x.norm() * e1;

  return v / v[0];
}
unittest
{
  auto v = compute_householder_vector(Matrix!double(4,1, [3,1,5,1]));
  v.modify_each( (double v) => cast(double)round(v*1000)/1000 );
  assert(v == [1,0.111,0.556,0.111]);
}

Matrix!T mulHouseholderGeneral(T)(Matrix!T v, Matrix!T A)
{
  return A - 2*v*(v.tv()*A) / dot(v,v);
}

Matrix!T mulGeneralHouseholder(T)(Matrix!T A, Matrix!T v)
{
  return A - 2*A*v*v.tv() / dot(v,v);
}

void QR(T)(Matrix!T M, out Matrix!T Q, out Matrix!T R)
{
  assert(M.rows >= M.cols);

  size_t m = M.rows;
  size_t n = M.cols;

  Q = Matrix!T(m,m, 0);
  foreach (i; 0 .. m)
    Q[i,i] = 1;

  R = Matrix!T(M);
  foreach (j; 0 .. n)
    {
      auto v = compute_householder_vector(R.submat(j,j,m-j,1));
      auto Rs = R.submat(j,j,m-j,n-j);
      Rs = mulHouseholderGeneral(v, Rs);

      auto Qs = Q.submat(0,j,m,m-j);
      Qs = mulGeneralHouseholder(Qs, v);
    }
}
unittest
{
  auto M = Matrix!double(3,3, [12, -51,  4,
                                6, 167, -68,
                               -4,  24, -41]);
  auto Q = Matrix!double();
  auto R = Matrix!double();
  QR(M, Q, R);

  auto MQR = Q*R;
  MQR.modify_each( (double v) => cast(double)round(v*1000)/1000 );
  assert(MQR == M);

  auto QQ = Q*Q.tv();
  QQ.modify_each( (double v) => cast(double)round(v*1000)/1000 );
  assert(QQ == Matrix!double.eye(3));

  R.modify_each( (double v) => cast(double)round(v*1000)/1000 );
  assert(R.diag().det() == R.det());
}

Matrix!T LS_solve(T)(Matrix!T A, Matrix!T b)
{
  assert(A.rows >= A.cols);
  assert(A.rows == b.rows);
  assert(b.cols == 1);

  auto Q = Matrix!T();
  auto R = Matrix!T();
  QR(A, Q, R);

  auto QA = Q.tv() * A;
  auto Qb = Q.tv() * b;
  size_t n = A.cols;

  return backward_substitution(QA.submat(0,0,n,n), Qb.submat(0,0,n,1));
}
unittest
{
  auto A = Matrix!double(5, 3, [4, -2, 1,
                                1, -1, 1,
                                0,  0, 1,
                                1,  1, 1,
                                4,  2, 1]);
  auto b = Matrix!double(5, 1, [4.05,
                                0.98,
                                0.01,
                                1.08,
                                3.95]);
  auto x = LS_solve(A, b);
  x.modify_each( (double v) => cast(double)round(v*1000)/1000 );
  assert(x == [0.994, -0.01, 0.025]);
}

void bidiagonalize(T)(Matrix!T A, out Matrix!T U, out Matrix!T B, out Matrix!T V)
{
  assert(A.rows >= A.cols);

  size_t m = A.rows;
  size_t n = A.cols;

  U = Matrix!T(m,m);
  B = Matrix!T(A);
  V = Matrix!T(n,n);

  foreach (i; 0 .. m)
    U[i,i] = 1;
  foreach (i; 0 .. n)
    V[i,i] = 1;

  foreach (j; 0 .. n-1)
    {
      auto u = compute_householder_vector(B.submat(j,j,m-j,1));
      auto Ms = B.submat(j,j,m-j,n-j);
      Ms = mulHouseholderGeneral(u, Ms);

      auto Us = U.submat(0,j,m,m-j);
      Us = mulGeneralHouseholder(Us, u);

      if (j < n-2)
        {
          auto v = compute_householder_vector(B.submat(j,j+1,1,n-j-1).tv());
          auto Ms2 = B.submat(j,j+1,m-j,n-j-1);
          Ms2 = mulGeneralHouseholder(Ms2, v);

          auto Vs = V.submat(j+1,1,n-j-1,n-1);
          Vs = mulHouseholderGeneral(v, Vs);
        }
    }
}
unittest
{
  auto M = Matrix!double(4,3, [ 1,  2,  3,
                                4,  5,  6,
                                7,  8,  9,
                               10, 11, 12]);
  auto U = Matrix!double();
  auto B = Matrix!double();
  auto V = Matrix!double();
  bidiagonalize(M, U, B, V);

  auto UBV = U * B * V;
  UBV.modify_each( (double v) => cast(double)round(v*1000)/1000 );
  assert(UBV == M);

  auto UU = U * U.tv();
  UU.modify_each( (double v) => cast(double)round(v*1000)/1000 );
  assert(UU == Matrix!double.eye(4));

  auto VV = V * V.tv();
  VV.modify_each( (double v) => cast(double)round(v*1000)/1000 );
  assert(VV == Matrix!double.eye(3));
}
