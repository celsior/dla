module matrix;

import std.traits;
import std.complex;
import std.stdio;
import std.algorithm: min,max;
import std.exception: RangeError, AssertError, assertThrown, assertNotThrown;
import std.conv;
import std.string: format;
import std.array: appender;
import std.format: formattedWrite;
import std.math: abs, round, sqrt;

enum Layout {Diagonal, General};

template isFloating(T)
{
  enum bool isFloating = is(T==cfloat) || is(T==cdouble) || is(T==creal) || is(T==float) || is(T==double) || is(T==real);
}

template isComplex(T)
{
  enum bool isComplex = isFloating!T
    || is(T==Complex!float) || is(T==Complex!double) || is(T==Complex!real);
}

template Zero(T)
{
  static if (isFloating!T)
    enum T Zero = 0.0;
  else static if (isComplex!T)
    enum T Zero = 0.0 + 0.0i;
  else static if (isIntegral!T)
    enum T Zero = 0;
  else static assert (false, "Zero: Type has no obvious zero: "~T.stringof);
}


interface IStorage(T) if (isNumeric!T)
{
  T opIndex(size_t i, size_t j);
  T opIndexAssign(T value, size_t i, size_t j);

  int indexesOf(int delegate(size_t, size_t) ops);

  size_t rows() @property;
  size_t cols() @property;
  size_t phys_size() @property;
  Layout layout() @property;
}


struct Matrix(T)
{
  IStorage!T _storage;

  static
  Matrix!T eye(size_t size)
  {
    return Matrix!T(size, size, 1, Layout.Diagonal);
  }

  static
  Matrix!T mulGeneralGeneral(T)(Matrix!T A, Matrix!T B)
  {
    auto res = Matrix!T(A.rows, B.cols, 0, Layout.General);

    foreach (j; 0 .. B.cols)
      foreach (k; 0 .. A.cols)
        foreach (i; 0 .. A.rows)
          res[i,j] += A[i,k] * B[k,j];

    return res;
  }

  static
  Matrix!T mulGeneralDiagonal(Matrix!T A, Matrix!T B)
  {
    auto res = Matrix!T(A.rows, B.cols, 0, Layout.General);

    foreach (j; 0 .. B.cols)
      foreach (i; 0 .. A.rows)
        res[i,j] = A[i,j] * B[j,j];

    return res;
  }

  static
  Matrix!T mulDiagonalGeneral(Matrix!T A, Matrix!T B)
  {
    auto res = Matrix!T(A.rows, B.cols, 0, Layout.General);

    foreach (j; 0 .. B.cols)
      foreach (i; 0 .. A.rows)
        res[i,j] = A[i,i] * B[i,j];

    return res;
  }

  static
  Matrix!T mulDiagonalDiagonal(Matrix!T A, Matrix!T B)
  {
    auto res = Matrix!T(A.rows, B.cols, 0, Layout.Diagonal);

    foreach (i; 0 .. min(A.rows, A.cols))
      res[i,i] = A[i,i] * B[i,i];

    return res;
  }

  this (size_t rows, size_t cols, T init = Zero!T,
        Layout layout = Layout.General)
  {
    switch (layout)
      {
      case Layout.Diagonal:
        _storage = new DiagonalStorage!T(rows, cols, init);
        break;
      default:
        _storage = new GeneralStorage!T(rows, cols, init);
        break;
      }
  }
  unittest
  {
    auto M1 = Matrix!int(3,3,1);
    assert (M1 == [1, 1, 1,
                   1, 1, 1,
                   1, 1, 1]);

    auto M2 = Matrix!int(3,3,1, Layout.Diagonal);
    assert (M2 == [1, 1, 1]);
  }

  this (size_t rows, size_t cols, T[] data,
        Layout layout = Layout.General)
  {
    switch (layout)
      {
      case Layout.Diagonal:
        _storage = new DiagonalStorage!T(rows, cols, data);
        break;
      default:
        _storage = new GeneralStorage!T(rows, cols, data);
        break;
      }
  }
  unittest
  {
    assertThrown!AssertError(Matrix!int(3,3, [1,1,1]));
    auto M1 = Matrix!int(3,3, [1,1,1], Layout.Diagonal);
    assert (M1 == [1, 1, 1]);
  }


  this (Matrix!T A)
  {
    switch (A.layout)
      {
      case Layout.Diagonal:
        _storage = new DiagonalStorage!T(A);
        break;
      default:
        _storage = new GeneralStorage!T(A);
        break;
      }
  }
  unittest
  {
    auto M1 = Matrix!int(3,3, [1,1,1], Layout.Diagonal);
    auto M2 = Matrix!int(M1);
    assert (M2 == [1, 1, 1]);
    M1[1,1] = 2;
    assert (M2[1,1] != 2);
  }

  this (IStorage!T storage)
  {
    _storage = storage;
  }

  void opAssign(Matrix!T A)
  {
    if (_storage is null)
      {
        switch (A.layout)
          {
          case Layout.Diagonal:
            _storage = new DiagonalStorage!T(A);
            break;
          default:
            _storage = new GeneralStorage!T(A);
            break;
          }
        return;
      }

    assert (rows == A.rows && cols == A.cols, "Incompatible shapes");
    foreach (size_t i, size_t j, T v; &A.elementsOf)
      this[i,j] = v;
  }

  T opIndex(size_t n)
  {
    assert (rows == 1 || cols == 1, "Not a vector");

    if (rows > 1)
      return _storage[n, 0];
    return _storage[0,n];
  }
  unittest
  {
    auto M1 = Matrix!int(3,3, [1,1,1], Layout.Diagonal);
    assertThrown!AssertError(M1[1], "Not a vector");

    auto M2 = Matrix!int(3,1, [1,2,3]);
    assert (M2[1] == 2);

    auto M3 = Matrix!int(1,3, [1,2,3]);
    assert (M3[1] == 2);
  }

  T opIndexAssign(T value, size_t n)
  {
    assert (rows == 1 || cols == 1, "Not a vector");

    if (rows > 1)
      return (_storage[n,0] = value);
    return (_storage[0,n] = value);
  }
  unittest
  {
    auto M1 = Matrix!int(3,3, [1,2,1], Layout.Diagonal);
    assertThrown!AssertError(M1[1] = 1, "Not a vector");

    auto M2 = Matrix!int(3,1, [1,1,1]);
    assertNotThrown(M2[1] = 2);
    assert(M2[1] == 2);

    auto M3 = Matrix!int(1,3, [1,1,1]);
    assertNotThrown(M3[1] = 2);
    assert(M3[1] == 2);
  }


  T opIndexOpAssign(string op)(T value, size_t n)
    if (op == "+" || op == "-" || op == "*" || op == "/")
  {
    assert (rows == 1 || cols == 1, "Not a vector");
    if (rows > 1)
      return mixin("_storage[n,0] = _storage[n,0]"~op~"value");
    return mixin("_storage[0,n] = _storage[0,n]"~op~"value");
  }
  unittest
  {
    auto M1 = Matrix!int(3,3, [1,2,1], Layout.Diagonal);
    assertThrown!AssertError(M1[1] += 1, "Not a vector");

    auto M2 = Matrix!int(3,1, [1,1,1]);
    assertNotThrown(M2[1] += 2);
    assert(M2[1] == 3);

    auto M3 = Matrix!int(1,3, [1,1,1]);
    assertNotThrown(M3[1] += 2);
    assert(M3[1] == 3);
  }


  T opIndex(size_t i, size_t j)
  {
    return _storage[i,j];
  }
  unittest
  {
    auto M1 = Matrix!int(3,3, [1,2,3], Layout.Diagonal);
    assert(M1[0,0] == 1);
    assert(M1[0,1] == 0);
    assert(M1[2,2] == 3);
    assertThrown!AssertError(M1[3,3], "Out of range");

    auto M2 = Matrix!int(3,1, [1,2,3]);
    assert(M2[0,0] == 1);
    assert(M2[1,0] == 2);
    assert(M2[2,0] == 3);
    assertThrown!AssertError(M2[3,0], "Out of range");

    auto M3 = Matrix!int(1,3, [1,2,3]);
    assert(M3[0,0] == 1);
    assert(M3[0,1] == 2);
    assert(M3[0,2] == 3);
    assertThrown!AssertError(M3[0,3], "Out of range");
  }

  T opIndexAssign(T value, size_t i, size_t j)
  {
    return _storage[i,j] = value;
  }
  unittest
  {
    auto M1 = Matrix!int(3,3, [1,1,1], Layout.Diagonal);
    M1[2,2] = 2;
    assert(M1[2,2] == 2);
  }


  T opIndexOpAssign(string op)(T value, size_t i, size_t j)
    if (op == "+" || op == "-" || op == "*" || op == "/")
  {
    return mixin("_storage[i, j] = _storage[i, j]"~op~"value");
  }
  unittest
  {
    auto M1 = Matrix!int(3,3, [1,1,1], Layout.Diagonal);
    M1[2,2] += 2;
    assert(M1[2,2] == 3);
  }


  auto opUnary(string op)()
    if (op == "+" || op == "-")
  {
    auto res = Matrix!T(this);
    static if (op == "+")
      return res;

    foreach (i,j,v; &elementsOf)
      res[i,j] = -v;
    return res;
  }
  unittest
  {
    auto M1 = Matrix!int(3,3, [1,1,1], Layout.Diagonal);
    auto M2 = -M1;
    assert (M2 == [-1, -1, -1]);
    auto M3 = +M1;
    assert (M3 == [1, 1, 1]);
  }

  auto opBinary(string op, T2 : Matrix)(T2 A)
    if (op == "*")
  {
    assert (this.cols == A.rows, "Dimension mismatch");

    if (layout == Layout.Diagonal && A.layout == Layout.General)
      return mulDiagonalGeneral(this, A);
    if (layout == Layout.General && A.layout == Layout.Diagonal)
      return mulGeneralDiagonal(this, A);
    if (layout == Layout.Diagonal && A.layout == Layout.Diagonal)
      return mulDiagonalDiagonal(this, A);

    return mulGeneralGeneral(this, A);
  }
  unittest
  {
    auto M1 = Matrix!int(3,3, [1,2,3], Layout.Diagonal);
    auto M2 = Matrix!int(3,3, [4,5,6], Layout.Diagonal);
    auto M3 = Matrix!int(3,3, [1,2,3,
                               4,5,6,
                               7,8,9]);

    auto M4 = Matrix!int(3,4, [1,2,3,4,
                               5,6,7,8,
                               9,10,11,12]);


    assert (M1*M2 == [4, 10, 18]);

    assert (M1*M3 == [1,  2,  3,
                      8,  10, 12,
                      21, 24, 27]);

    assert (M3*M1 == [1,  4,  9,
                      4,  10, 18,
                      7,  16, 27]);

    assert (M1*M4 == [1,   2,   3,    4,
                      10,  12,  14,  16,
                      27,  30,  33,  36]);

    assert (M3*M4 == [38,   44,   50,   56,
                      83,   98,   113,  128,
                      128,  152,  176,  200]);

    assertThrown!AssertError(M4*M3, "Dimension mismatch");
  }

  auto opBinary(string op, T2)(T2 s)
    if ((op == "*" || op == "/") && isNumeric!T2)
  {
    auto res = Matrix!T(this);
    foreach (size_t i, size_t j, T v; &elementsOf)
      res[i,j] = mixin("v" ~ op ~ "s");
    return res;
  }
  auto opBinaryRight(string op, T2)(T2 s)
    if (op == "*" && isNumeric!T2)
  {
    auto res = Matrix!T(this);
    foreach (size_t i, size_t j, T v; &elementsOf)
      res[i,j] = s * v;
    return res;
  }
  unittest
  {
    auto M1 = Matrix!int(3,3, [2,4,6], Layout.Diagonal);

    auto M3 = M1 * 2;
    auto M4 = 2 * M1;
    auto M5 = M1 / 2;

    assert(M3 == [4, 8, 12]);
    assert(M4 == [4, 8, 12]);
    assert(M5 == [1, 2, 3]);
  }

  auto opBinary(string op, T2)(T2 s)
    if ((op == "-" || op == "+") && isNumeric!T2)
  {
    auto res = Matrix!T(rows, cols, 0, Layout.General);
    foreach (size_t i; 0..rows)
      foreach (size_t j; 0..cols)
        res[i,j] = mixin("this[i,j]" ~ op ~ "s");
    return res;
  }
  auto opBinaryRight(string op, T2)(T2 s)
    if ((op == "-" || op == "+") && isNumeric!T2)
  {
    auto res = Matrix!T(rows, cols, 0, Layout.General);
    foreach (size_t i; 0..rows)
      foreach (size_t j; 0..cols)
        res[i,j] = mixin("s" ~ op ~ "this[i,j]");
    return res;
  }
  unittest
  {
    auto M1 = Matrix!int(2,2, [2,4], Layout.Diagonal);
    auto M2 = 1-M1;
    auto M3 = M1-1;

    assert(M2 == [-1, 1,
                  1, -3]);
    assert(M3 == [ 1, -1,
                  -1,  3]);
  }

  auto opBinary(string op, T2 : Matrix)(T2 A)
    if (op == "/")
  {
    return this*inv(A);
  }

  auto opBinary(string op, T2 : Matrix)(T2 A)
    if (op == "+" || op == "-")
  {
    assert (rows == A.rows && cols == A.cols, "Dimension mismatch");

    if (layout == Layout.Diagonal && A.layout == Layout.Diagonal)
      {
        auto res = Matrix!T(rows, cols, 0, Layout.Diagonal);
        foreach (i; 0 .. rows)
          res[i,i] = mixin("_storage[i,i]"~op~"A[i,i]");
        return res;
      }

    auto res = Matrix!T(rows, cols, 0, Layout.General);
    foreach (i; 0 .. rows)
      foreach (j; 0 .. cols)
        res[i,j] = mixin("_storage[i,j]"~op~"A[i,j]");
    return res;
  }
  unittest
  {
    auto M1 = Matrix!int(3,3, [1,2,3], Layout.Diagonal);
    auto M2 = Matrix!int(3,3, [4,5,6], Layout.Diagonal);

    auto M4 = Matrix!int(3,4, [1,2,3,4,
                               5,6,7,8,
                               9,10,11,12]);
    auto M5 = Matrix!int(3,4, [1,1,1,1,
                               2,2,2,2,
                               3,3,3,3]);

    assert(M1-M2 == [-3,-3,-3]);
    assert(M4-M5 == [0, 1, 2, 3,
                     3, 4, 5, 6,
                     6, 7, 8, 9]);

    assertThrown!AssertError(M2+M5, "Dimension mismatch");
  }

  bool opEquals(Matrix!T A)
  {
    if (A.rows != rows || A.cols != cols)
      return false;

    foreach (size_t i; 0..rows)
      foreach (size_t j; 0..cols)
        if (A[i,j] != this[i,j])
          return false;

    return true;
  }
  unittest
  {
    auto M1 = Matrix!int(3,3, [1,2,3], Layout.Diagonal);
    auto M2 = Matrix!int(3,3, [1,0,0,
                               0,2,0,
                               0,0,3], Layout.General);
    auto M3 = Matrix!int(3,3, [4,5,6], Layout.Diagonal);
    auto M4 = Matrix!int(4,4, [4,5,6,0], Layout.Diagonal);

    assert(M1 == M1);
    assert(M1 == M2);
    assert(M2 != M3);
    assert(M3 != M4);
  }


  bool opEquals(T[] A)
  {
    if (A.length != _storage.phys_size)
      return false;

    foreach (size_t n, T v; &elementsOf)
      if (A[n] != v)
        return false;

    return true;
  }
  unittest
  {
    auto M1 = Matrix!int(3,3, [1,2,3], Layout.Diagonal);
    auto M2 = Matrix!int(3,3, [1,0,0,
                               0,2,0,
                               0,0,3], Layout.General);

    assert(M1 == [1,2,3]);
    assert(M2 == [1,0,0,0,2,0,0,0,3]);

    auto M3 = Matrix!int(3, 1, [2,2,1]);
    assert(M3 != [1,2,3]);
  }

  size_t rows() @property
  {
    return _storage.rows;
  }

  size_t cols() @property
  {
    return _storage.cols;
  }

  Layout layout() @property
  {
    return _storage.layout;
  }


  Matrix!T2 as(T2)()
  {
    return Matrix!T2(new CastedStorage!(T2,T)(_storage));
  }
  unittest
  {
    auto m1 = Matrix!int(3, 3, 4);
    auto m2 = Matrix!double(3, 3, [1, 0.5, 0.25], Layout.Diagonal);
    auto m3 = m1.as!double * m2;

    assert (m3 == [4, 2, 1,
                   4, 2, 1,
                   4, 2, 1]);
  }

  Matrix!T submat(size_t start_row, size_t start_col, size_t rows, size_t cols)
  {
    return Matrix!T(new SubmatrixStorage!T(_storage, start_row, start_col, rows, cols));
  }
  unittest
  {
    auto M1 = Matrix!int(3,4, [1,2,3,4,
                               5,6,7,8,
                               9,10,11,12]);

    auto M2 = M1.submat(0,1,2,2);
    assert(M2 == [2, 3,
                  6, 7]);
    M2[1,1] = 10;
    assert(M1[1,2] == 10);
  }

  Matrix!T row(size_t row)
  {
    return Matrix!T(new SubmatrixStorage!T(_storage, row, 0, 1, cols));
  }
  unittest
  {
    auto M1 = Matrix!int(3,4, [1,2,3,4,
                               5,6,7,8,
                               9,10,11,12]);
    assert(M1.row(0) == [1,2,3,4]);
    assert(M1.row(1) == [5,6,7,8]);
    assert(M1.row(2) == [9,10,11,12]);

    auto M2 = Matrix!int(3,3, [1,2,3], Layout.Diagonal);
    assert(M2.row(1) == [0, 2, 0]);
  }

  Matrix!T col(size_t col)
  {
    return Matrix!T(new SubmatrixStorage!T(_storage, 0, col, rows, 1));
  }
  unittest
  {
    auto M1 = Matrix!int(3,4, [1,2,3,4,
                               5,6,7,8,
                               9,10,11,12]);
    assert(M1.col(0) == [1,5,9]);
  }

  Matrix!T diag()
  {
    return Matrix!T(new DiagonalView!T(_storage));
  }
  unittest
  {
    auto M1 = Matrix!int(3,3, [1,2,3], Layout.Diagonal);
    auto M2 = Matrix!int(3,3, [1,2,3,
                               4,5,6,
                               7,8,9]);
    auto M3 = M2.diag();

    assert (M1*M3 == [1, 10, 27]);
  }

  int elementsOf(int delegate(T) ops)
  {
    int ctl = 0;
    foreach (size_t i, size_t j; &_storage.indexesOf)
      {
        ctl = ops(_storage[i,j]);
        if (ctl)
          break;
      }
    return ctl;
  }

  int elementsOf(int delegate(size_t, T) ops)
  {
    int ctl = 0;
    size_t n = 0;
    foreach (size_t i, size_t j; &_storage.indexesOf)
      {
        ctl = ops(n, _storage[i,j]);
        if (ctl)
          break;
        ++n;
      }
    return ctl;
  }


  int elementsOf(int delegate(size_t, size_t, T) ops)
  {
    int ctl = 0;
    foreach (size_t i, size_t j; &_storage.indexesOf)
      {
        ctl = ops(i, j, _storage[i,j]);
        if (ctl)
          break;
      }
    return ctl;
  }

  void copyTo(IStorage!T A)
  {
    foreach (size_t i, size_t j, T v; &elementsOf)
      A[i,j] = v;
  }

  T[] export_data()
  {
    T[] res = new T[_storage.phys_size];
    size_t n = 0;
    foreach (T v; &elementsOf)
      {
        res[n] = v;
        ++n;
      }
    return res;
  }

  string toString()
  {
    size_t max_elem_len = 0;
    string elem_fmt = "%s  ";

    foreach (size_t i; 0..rows)
      foreach (size_t j; 0..cols)
        max_elem_len = max(max_elem_len, format(elem_fmt, this[i,j]).length);

    string fmt = format("%%%ss", max_elem_len);

    auto res = appender!string();
    foreach (size_t i; 0..rows)
      {
        foreach (size_t j; 0..cols)
          formattedWrite(res, fmt, format(elem_fmt, this[i,j]));
        formattedWrite(res, "\n");
      }

    return res.data;
  }
  unittest
  {
    auto M1 = Matrix!double(3,4, [1,2,3,4,
                               5,6,7,8,
                               9,10,11,12]);

    assertNotThrown(M1.toString());
  }

  Matrix!T tv()
  {
    return Matrix!T(new TransposedView!T(_storage));
  }
  unittest
  {
    auto M1 = Matrix!double(3,4, [1,2,3,4,
                               5,6,7,8,
                               9,10,11,12]);
    auto M2 = M1.tv();
    assert (M2 == [1, 5, 9,
                   2, 6, 10,
                   3, 7, 11,
                   4, 8, 12]);
    M2[1,2] = 0;
    assert (M1[2,1] == 0);
  }

  Matrix!T t()
  {
    auto res = Matrix!T(cols, rows, 0, layout);
    foreach (size_t i,  size_t j, T v; &elementsOf)
      res[j,i] = v;
    return res;
  }
  unittest
  {
    auto M1 = Matrix!double(3,4, [1,2,3,4,
                               5,6,7,8,
                               9,10,11,12]);
    auto M2 = M1.t();
    assert (M2 == [1, 5, 9,
                   2, 6, 10,
                   3, 7, 11,
                   4, 8, 12]);

    auto M3 = Matrix!double(3,4, [1,2,3], Layout.Diagonal);
    auto M4 = M3.t();
    assert (M4.row(3) == [0,0,0]);
  }

  Matrix!T minor_view(size_t excl_i, size_t excl_j)
  {
    return Matrix!T(new MinorView!T(_storage, excl_i, excl_j));
  }
  unittest
  {
    auto M1 = Matrix!int(3,3, [1,2,3,
                               4,5,6,
                               7,8,9]);

    assert (M1.minor_view(0,0) == [5,6,
                                   8,9]);
    assert (M1.minor_view(1,1) == [1,3,
                                   7,9]);
    assert (M1.minor_view(2,1) == [1,3,
                                   4,6]);
  }

  T det()
  {
    assert (rows == cols, "Not a square matrix");

    if (rows == 1)
      return this[0,0];
    if (rows == 2)
      return this[0,0] * this[1,1]  -  this[0,1] * this[1,0];

    if (layout == Layout.Diagonal)
      return this[0,0] * minor_view(0,0).det();

    T res = 0;
    foreach (size_t i; 0..cols)
      res += (i % 2 == 0 ? 1 : -1) * this[0,i] * minor_view(0,i).det();
    return res;
  }
  unittest
  {
    auto M1 = Matrix!int(3,3, [1,2,3], Layout.Diagonal);
    assert (M1.det() == 6);

    auto M2 = Matrix!int(2,2, [1,2,
                               4,5]);
    assert (M2.det() == -3);

    auto M3 = Matrix!int(3,3, [1,2,3,
                               4,5,6,
                               7,8,9]);

    assert (M3.det() == 0);
  }

  S norm(S = double)(string p = "2")
  {
    S res = 0;
    if (p == "2" || p == "fro" || p == "eu")
      {
        foreach (T v; &elementsOf)
          res += v*v;
        res = sqrt(res);
      }

    return res;
  }
  unittest
  {
    auto M = Matrix!int(3,3, [1,2,3,
                              4,5,6,
                              7,8,9]);
    assert(round(M.norm()*1000)/1000 == 16.882);
  }

  void modify_each(T delegate(T v) dg)
  {
    foreach (size_t i, size_t j, T v; &elementsOf)
      this[i,j] = dg(v);
  }

  void swap_rows(size_t i, size_t j)
  {
    assert(layout == Layout.General);

    T tmp;
    foreach (k; 0 .. cols)
      {
        tmp = this[i,k];
        this[i,k] = this[j,k];
        this[j,k] = tmp;
      }
  }
  unittest
  {
    auto M = Matrix!int(3,3, [1,2,3,
                              4,5,6,
                              7,8,9]);
    M.swap_rows(0,2);
    assert(M == [7,8,9,
                 4,5,6,
                 1,2,3]);
  }

  void swap_cols(size_t i, size_t j)
  {
    assert(layout == Layout.General);

    T tmp;
    foreach (k; 0 .. rows)
      {
        tmp = this[k,i];
        this[k,i] = this[k,j];
        this[k,j] = tmp;
      }
  }
  unittest
  {
    auto M = Matrix!int(3,3, [1,2,3,
                              4,5,6,
                              7,8,9]);
    M.swap_cols(0,2);
    assert(M == [3,2,1,
                 6,5,4,
                 9,8,7]);
  }

  bool is_strictly_diagonally_dominant()
  {
    if (layout == Layout.Diagonal)
      return true;

    size_t n = rows;

    foreach (i; 0..n)
      {
        T row_sum = 0;
        foreach (j; 0..n)
          if (j != i)
            row_sum += abs(this[i,j]);

        if (abs(this[i,i]) < row_sum)
          return false;
      }

    return true;
  }
}

T dot(T)(Matrix!T M1, Matrix!T M2)
{
  assert(M1.rows == M2.rows && M1.cols == M2.cols);
  T res = 0;
  foreach (i; 0 .. M1.rows)
    foreach (j; 0 .. M1.cols)
      res += M1[i,j] * M2[i,j];
  return res;
}
unittest
{
  auto M1 = Matrix!double(3,1, [1,2,3]);
  assert(dot(M1, M1) == 14);
}

mixin template StorageData(T, Layout lay)
{
  T[] _data;
  size_t _rows;
  size_t _cols;
  Layout _layout = lay;

  size_t rows() @property
  {
    return _rows;
  }

  size_t cols() @property
  {
    return _cols;
  }

  Layout layout() @property
  {
    return _layout;
  }

  size_t phys_size() @property
  {
    return _data.length;
  }
}

mixin template GeneralIndexes()
{
  int indexesOf(int delegate(size_t, size_t) ops)
  {
    int ctl = 0;
    foreach (size_t i; 0..rows)
      foreach (size_t j; 0..cols)
        {
          ctl = ops(i, j);
          if (ctl)
            return ctl;
        }
    return ctl;
  }
}

mixin template DiagonalIndexes()
{
  int indexesOf(int delegate(size_t, size_t) ops)
  {
    int ctl = 0;
    foreach (size_t i; 0.._n)
      {
        ctl = ops(i, i);
        if (ctl)
          break;
      }
    return ctl;
  }
}

class GeneralStorage(T) : IStorage!T
{
  mixin StorageData!(T, Layout.General);

  this (size_t rows, size_t cols, T init = Zero!T)
  {
    assert(rows > 0 && cols > 0);
    _rows = rows;
    _cols = cols;
    _data = new T[rows*cols];
    _data[] = init;
  }

  this (size_t rows, size_t cols, T[] data)
  {
    assert(rows > 0 && cols > 0 && data.length == rows*cols, "Initializer size is inconsistent with matrix size");

    _rows = rows;
    _cols = cols;
    _data = data.dup;
  }

  this (Matrix!T A)
  {
    _rows = A.rows;
    _cols = A.cols;
    _data = new T[_rows*_cols];

    A.copyTo(this);
  }

  size_t index(size_t i, size_t j)
  {
    assert (i < _rows && j < _cols, "Out of range");
    return i*_cols + j;
  }

  T opIndex(size_t i, size_t j)
  {
    return _data[index(i,j)];
  }

  T opIndexAssign(T value, size_t i, size_t j)
  {
    return _data[index(i,j)] = value;
  }

  mixin GeneralIndexes;
}


class DiagonalStorage(T) : IStorage!T
{
  mixin StorageData!(T, Layout.Diagonal);
  size_t _n;

  this (size_t rows, size_t cols, T init = Zero!T)
  {
    assert(rows > 0 && cols > 0);
    _rows = rows;
    _cols = cols;
    _n = min(rows, cols);
    _data = new T[_n];
    _data[] = init;
  }

  this (size_t rows, size_t cols, T[] data)
  {
    assert(rows > 0 && cols > 0 && data.length == min(rows,cols));

    _rows = rows;
    _cols = cols;
    _n = min(rows, cols);
    _data = data.dup;
  }

  this (Matrix!T A)
  {
    _rows = A.rows;
    _cols = A.cols;
    _n = min(rows, cols);
    _data = new T[_n];

    A.copyTo(this);
  }

  T opIndex(size_t i, size_t j)
  {
    assert (i < _rows && j < _cols, "Out of range");
    if (i == j)
      return _data[i];
    return 0;
  }

  T opIndexAssign(T value, size_t i, size_t j)
  {
    assert (i < _rows && j < _cols, "Out of range");
    if (i == j)
      return _data[i] = value;

    throw new RangeError("Index is out of storage");
  }

  mixin DiagonalIndexes;
}

class CastedStorage(T, Ts) : IStorage!T
{
  IStorage!Ts _storage;

  size_t rows() {return _storage.rows;}
  size_t cols() {return _storage.cols;}
  Layout layout() {return _storage.layout;}
  size_t phys_size() {return _storage.phys_size;}

  this(IStorage!Ts storage)
  {
    _storage = storage;
  }

  T opIndex(size_t i, size_t j)
  {
    return to!T(_storage[i,j]);
  }

  T opIndexAssign(T value, size_t i, size_t j)
  {
    return to!T(_storage[i,j] = to!Ts(value));
  }

  mixin GeneralIndexes;
}

class SubmatrixStorage(T) : IStorage!T
{
  IStorage!T _storage;
  size_t _start_row;
  size_t _start_col;
  size_t _rows;
  size_t _cols;

  size_t rows()
  {
    return _rows;
  };

  size_t cols()
  {
    return _cols;
  };

  Layout layout() {return Layout.General;};
  size_t phys_size() {return _rows*_cols;}

  this(IStorage!T storage, size_t start_row, size_t start_col, size_t rows, size_t cols)
  {
    _storage = storage;
    _start_row = start_row;
    _start_col = start_col;
    _rows = rows;
    _cols = cols;
  }

  T opIndex(size_t i, size_t j)
  {
    return _storage[i + _start_row, j + _start_col];
  }

  T opIndexAssign(T value, size_t i, size_t j)
  {
    return _storage[i + _start_row, j + _start_col] = value;
  }

  mixin GeneralIndexes;
}


class DiagonalView(T) : IStorage!T
{
  IStorage!T _storage;
  size_t _n;

  size_t rows() {return _storage.rows;};
  size_t cols() {return _storage.cols;};
  Layout layout() {return Layout.Diagonal;};
  size_t phys_size() {return _n;}


  this(IStorage!T storage)
  {
    _storage = storage;
    _n = min(storage.rows, storage.cols);
  }

  T opIndex(size_t i, size_t j)
  {
    assert (i < rows && j < cols, "Out of range");
    if (i == j)
      return _storage[i,i];
    return 0;
  }

  T opIndexAssign(T value, size_t i, size_t j)
  {
    assert (i < rows && j < cols, "Out of range");
    if (i == j)
      return (_storage[i,i] = value);

    throw new RangeError("Index is out of storage");
  }

  mixin DiagonalIndexes;
}

class TransposedView(T) : IStorage!T
{
  IStorage!T _storage;

  size_t rows() {return _storage.cols;};
  size_t cols() {return _storage.rows;};
  Layout layout() {return _storage.layout;};
  size_t phys_size() {return _storage.phys_size;}

  this(IStorage!T storage)
  {
    _storage = storage;
  }

  T opIndex(size_t i, size_t j)
  {
    assert (i < rows && j < cols, "Out of range");
    return _storage[j,i];
    return 0;
  }

  T opIndexAssign(T value, size_t i, size_t j)
  {
    assert (i < rows && j < cols, "Out of range");
    return (_storage[j,i] = value);
  }

  mixin GeneralIndexes;
}

class MinorView(T) : IStorage!T
{
  IStorage!T _storage;
  size_t _excl_i;
  size_t _excl_j;

  size_t rows() {return _storage.cols-1;};
  size_t cols() {return _storage.rows-1;};
  Layout layout() {return Layout.General;};
  size_t phys_size() {return rows*cols;}

  this(IStorage!T storage, size_t excl_i, size_t excl_j)
  {
    _storage = storage;
    _excl_i = excl_i;
    _excl_j = excl_j;
  }

  T opIndex(size_t i, size_t j)
  {
    assert (i < rows && j < cols, "Out of range");
    return _storage[i < _excl_i ? i : i+1, j < _excl_j ? j : j+1];
  }

  T opIndexAssign(T value, size_t i, size_t j)
  {
    assert (i < rows && j < cols, "Out of range");
    return (_storage[i < _excl_i ? i : i+1, j < _excl_j ? j : j+1] = value);
  }

  mixin GeneralIndexes;
}


unittest
{
    auto M1 = Matrix!int(3,4, [1,2,3,4,
                               5,6,7,8,
                               9,10,11,12]);

    auto M2 = Matrix!double(3,4, [1,2,3,4,
                               5,6,7,8,
                               9,10,11,12]);
}
